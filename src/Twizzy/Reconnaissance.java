package Twizzy;

import java.io.IOException;
import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

public class Reconnaissance {

	public static void main(String args[]) throws IOException  { 
		// Chargement de la librairie OpenCV  
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );


		// Chargement de l'image depuis le fichier
		String objectfile ="Panneau50.jpg";
		Mat m= utils.LectureImage(objectfile);
		utils.Imshow("Originale", m);
		Mat hsv_image = Mat.zeros(m.size(), m.type());
		Imgproc.cvtColor(m, hsv_image, Imgproc.COLOR_BGR2HSV);
		
		utils.extrairePanneau(objectfile);
		//utils.contours(objectfile);
		
	}
}
